/*-----------------------------------

- CamFun -

created by FV iMAGINATION © 2015
for CodeCanyon.net

-----------------------------------*/


import UIKit


class CropImageVC: UIViewController {

   /* Views */
    @IBOutlet var containerView: UIView!
    @IBOutlet var bkgImage: UIImageView!
    @IBOutlet var imageToBeCropped: UIImageView!
    
    @IBOutlet var bkgsScrollView: UIScrollView!
    

    // Buttons
    @IBOutlet var bkgButtons: [UIButton]!
    var buttTAG = 0

    
    

override var prefersStatusBarHidden : Bool {
    return true;
}
func navigationController(_ navigationController: UINavigationController, willShowViewController viewController: UIViewController, animated: Bool) {
    UIApplication.shared.setStatusBarHidden(true, with: UIStatusBarAnimation.none)
}
    
    
override func viewDidLoad() {
        super.viewDidLoad()
    
    // Get the taken Image
    imageToBeCropped.image = takenImage
    
    // Resize the container View accordingly to the screen's width
    containerView.frame = CGRect(x: 0, y: 44, width: self.view.frame.size.width, height: self.view.frame.size.width)
    
    // Set image in the middle
    imageToBeCropped.center = CGPoint(x: containerView.frame.size.width/2, y: containerView.frame.size.height/2)
    
    // Resize bkgScrollView
    bkgsScrollView.frame = CGRect(x: 0, y: containerView.frame.size.height+80, width: view.frame.size.width, height: 66)
    let buttonsAmount = CGFloat(bkgButtons.count)
    bkgsScrollView.contentSize = CGSize(width: 74 * buttonsAmount, height: 66)
    
    // Prepare BKG Buttons
    for button in bkgButtons {
        button.addTarget(self, action: #selector(bkgTapped(_:)), for: .touchUpInside)
    }
    
    
    /*
    // CONSOLE LOGS
    println("imageTAG (on CropVC): \(imageTAG)")
    */
}

    
func bkgTapped(_ sender: UIButton) {
    let button = sender as UIButton
    bkgImage.image = button.backgroundImage(for: .normal)
}


    
    
// MARK: - CROP BUTTON
@IBAction func okCropButt(_ sender: AnyObject) {
    showHUD()
    Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(cropImage), userInfo: nil, repeats: false)
}
    
func cropImage() {
    // Crop the image into the square containerView
    let rect:CGRect = containerView.bounds
    UIGraphicsBeginImageContextWithOptions(rect.size, true, 0.0)
    containerView.drawHierarchy(in: containerView.bounds, afterScreenUpdates: false)
    croppedImage = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
        
    if imageTAG > -1 {
        switch imageTAG {
            case 0:  image1 = croppedImage;  break
            case 1:  image2 = croppedImage;  break
            case 2:  image3 = croppedImage;  break
            case 3:  image4 = croppedImage;  break
        default:break   }
            
    // Go to Edit Collage VC
    let ecVC = self.storyboard?.instantiateViewController(withIdentifier: "EditCollageVC") as! EditCollageVC
    ecVC.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
    present(ecVC, animated: true, completion: nil)
    
        
    // Go to Image Editor
    } else {
        let imageEditorVC = self.storyboard?.instantiateViewController(withIdentifier: "ImageEditorVC") as! ImageEditorVC
        present(imageEditorVC, animated: true, completion: nil)
    }

}

    
    
// MARK: - PINCH GESTURES ON THE IMAGE
@IBAction func zoomImage(_ sender: UIPinchGestureRecognizer) {
        sender.view?.transform = sender.view!.transform.scaledBy(x: sender.scale, y: sender.scale)
        sender.scale = 1
}
   
// MARK: - PAN GESTURE ON THE IMAGE
@IBAction func moveImage(_ sender: UIPanGestureRecognizer) {
    let translation: CGPoint =  sender.translation(in: self.view)
        sender.view?.center = CGPoint(x: sender.view!.center.x +  translation.x, y: sender.view!.center.y + translation.y)
        sender.setTranslation(CGPoint(x: 0, y: 0), in: self.view)
}

// MARK: - ROTATION GESTURE ON THE IMAGE
@IBAction func rotateImage(_ sender: UIRotationGestureRecognizer) {
    if sender.state == UIGestureRecognizerState.began ||
       sender.state == UIGestureRecognizerState.changed {
            sender.view!.transform = sender.view!.transform.rotated(by: sender.rotation)
            sender.rotation = 0
    }
}
    
    

    
override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
}
}
