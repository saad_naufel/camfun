/*-----------------------------------

  - CamFun -

  created by FV iMAGINATION © 2015
  for CodeCanyon.net

-----------------------------------*/

import Foundation
import UIKit


// Replace the red string below with the name you'll give to your own version of this app
var APP_NAME = "CamFun"

// Replace xxxx with your own App ID (you can get it on your app's page in iTunes Connect, click "More->About this App")
var APP_ID = "xxxxxx"


var REPORT_EMAIL_SUBJECT = "Report Inappropriate Image"

// Replace the email address below with your own one (we suggest you to create a dedicated email address to have users able to report inappropriate or offensive images)
var REPORT_EMAIL_ADDRESS = "report@mydomain.com"


// Replace the Test ID below with your own AdMob Unit ID (you can get it on http://apps.admob.com - setup an account if you don't have it yet and create a Banner Unit ID (the website has a guide that explains how to do that)
var ADMOB_UNIT_ID = "ca-app-pub-3940256099942544/2934735716"


// You should setup a free account on Instagram, grab its username form the end of its link and replace this one below with your own one. This is responsible for the Instagram button in the Home VC
var INSTAGRAM_USERNAME = "photolabapp"


// Edit these strings with your own support email address and message body
var MY_SUPPORT_EMAIL_ADDRESS = "support@mydomain.com"
var SUPPORT_MAIL_SUBJECT = "Support Request"
var SUPPORT_MESSAGE_BODY = "Write your feedbacks/suggestions below: \n"


// You can edit these strings as you wish, they are responsible for the sharing section on twitter and Facebook
var SHARING_SUBJECT = "My \(APP_NAME) Picture!"
var SHARING_MESSAGE = "Hi there, check this picture out, I've made it with #\(APP_NAME)"




// Array of Colors (check settings below)
let colorsList = [
    red, orange, yellow, lightGreen, mint, aqua, blueJeans, lavander,
    darkPurple, pink, darkRed,  paleWhite, lightGray, mediumGray, darkGray
]


// DEFINE PALETTE OF COLORS  (you can edit the RGBA values of these colors if you want, just leave "255.0" the wauy it is, edit only the other values.
// Alpha value goes from 0.0 to 1.0 - RGB values go from 0.0 to 255.0
var red = UIColor(red: 237.0/255.0, green: 85.0/255.0, blue: 100.0/255.0, alpha: 1.0)
var orange = UIColor(red: 250.0/255.0, green: 110.0/255.0, blue: 82.0/255.0, alpha: 1.0)
var yellow = UIColor(red: 255.0/255.0, green: 207.0/255.0, blue: 85.0/255.0, alpha: 1.0)
var lightGreen = UIColor(red: 160.0/255.0, green: 212.0/255.0, blue: 104.0/255.0, alpha: 1.0)
var mint = UIColor(red: 72.0/255.0, green: 207.0/255.0, blue: 174.0/255.0, alpha: 1.0)
var aqua = UIColor(red: 79.0/255.0, green: 192.0/255.0, blue: 232.0/255.0, alpha: 1.0)
var blueJeans = UIColor(red: 93.0/255.0, green: 155.0/255.0, blue: 236.0/255.0, alpha: 1.0)
var lavander = UIColor(red: 172.0/255.0, green: 146.0/255.0, blue: 237.0/255.0, alpha: 1.0)
var darkPurple = UIColor(red: 150.0/255.0, green: 123.0/255.0, blue: 220.0/255.0, alpha: 1.0)
var pink = UIColor(red: 236.0/255.0, green: 136.0/255.0, blue: 192.0/255.0, alpha: 1.0)
var darkRed = UIColor(red: 218.0/255.0, green: 69.0/255.0, blue: 83.0/255.0, alpha: 1.0)
var paleWhite = UIColor(red: 246.0/255.0, green: 247.0/255.0, blue: 251.0/255.0, alpha: 1.0)
var lightGray = UIColor(red: 230.0/255.0, green: 233.0/255.0, blue: 238.0/255.0, alpha: 1.0)
var mediumGray = UIColor(red: 204.0/255.0, green: 208.0/255.0, blue: 217.0/255.0, alpha: 1.0)
var darkGray = UIColor(red: 67.0/255.0, green: 74.0/255.0, blue: 84.0/255.0, alpha: 1.0)
//------------------------------------------------------------------------------------------------------------------




// HUD View
let hudView = UIView(frame: CGRect(x: 0, y: 0, width: 80, height: 80))
let indicatorView = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 80, height: 80))
extension UIViewController {
    func showHUD() {
        hudView.center = CGPoint(x: view.frame.size.width/2, y: view.frame.size.height/2)
        hudView.backgroundColor = UIColor.darkGray
        hudView.alpha = 0.9
        hudView.layer.cornerRadius = hudView.bounds.size.width/2
        
        indicatorView.center = CGPoint(x: hudView.frame.size.width/2, y: hudView.frame.size.height/2)
        indicatorView.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.white
        hudView.addSubview(indicatorView)
        indicatorView.startAnimating()
        view.addSubview(hudView)
    }
    
    func hideHUD() { hudView.removeFromSuperview() }
    
    func simpleAlert(_ mess:String) {
        UIAlertView(title: APP_NAME, message: mess, delegate: nil, cancelButtonTitle: "OK").show()
    }
}





// Replace these 2 strings with your own App and Client keys you'll get from http://back4app.com
let PARSE_APP_KEY = "4rfxsk2Tax2R9naIronLbjQlWGWKqPkrXndkEbeH"
let PARSE_CLIENT_KEY = "6mgg6oE1CZmbylzl8T0KgifZnqPJFHHpz4KKGsJZ"





/********** DO NOT EDIT THE CODE BELOW *************/
var editedImage: UIImage?
var takenImage: UIImage?
var croppedImage: UIImage?

var collageFrameNr = 0
var imageTAG = -1

// For saving data locally
var DEFAULTS = UserDefaults.standard

// Load BOOLs
var saveImageToLibrary = DEFAULTS.bool(forKey: "saveImageToLibrary")
var rateAppMade: Bool = DEFAULTS.bool(forKey: "rateAppMade")



// POSTS CLASS:
var POSTS_CLASS_NAME = "Posts"
var POSTS_IMAGE_FILE = "postImageFile"
var POSTS_USER_AVATAR = "postUserAvatar"
var POSTS_LIKES = "postLikes"
var POSTS_USER = "postUser" // Pointer
var POSTS_USERNAME = "postUsername"
var POSTS_POSITION = "postPosition"
var POSTS_CREATED_AT = "createdAt"
var POSTS_UPDATED_AT = "updatedAt"

// USER CLASS:
var USER_USERNAME = "username"
var USER_AVATAR = "avatar"


// DO NOT edit these variables, they are responsible for showing/hiding tools in the Image Editor Controller
var FRAMESTOOL_NAME = "Frames"
var STICKERSTOOL_NAME = "Stickers"
var FILTERSTOOL_NAME = "Filters"
var TEXTURESTOOL_NAME = "Textures"
var ADJUSTMENTTOOL_NAME = "Adjustment"

















