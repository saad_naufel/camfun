/*-----------------------------------

- CamFun -

created by FV iMAGINATION © 2015
for CodeCanyon.net

-----------------------------------*/

import UIKit
import AVFoundation
import QuartzCore
import CoreImage
import CoreMedia



class CameraVC: UIViewController,
UINavigationControllerDelegate,
UIImagePickerControllerDelegate
{

    /* Variables */
    let captureSession = AVCaptureSession()
    var previewLayer : AVCaptureVideoPreviewLayer?
    var stillImageOutput: AVCaptureStillImageOutput?
    var frontCameraDev: AVCaptureDevice?
    var backCameraDev: AVCaptureDevice?
    var videoInputDevice: AVCaptureDeviceInput?
    
    /* Camera Booleans */
    var FrontCamera = false
    var FlashON = false
    var gridON = false
    var exposureON = false
    
    /* Views */
    @IBOutlet weak var imagePreviewView: UIView!
    @IBOutlet weak var captureImage: UIImageView!
    @IBOutlet var gridImage: UIImageView!
    
    @IBOutlet var exposureView: UIView!
    @IBOutlet var exposureSlider: UISlider!
    
    /* Buttons */
    @IBOutlet var snapPicOutlet: UIButton!
    @IBOutlet var gridButtOutlet: UIButton!
    @IBOutlet weak var flashOutlet: UIButton!
    @IBOutlet var gridOutlet: UIButton!


    
    
    
// Hide the status bar
override var prefersStatusBarHidden : Bool {
    return true;
}
func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
    UIApplication.shared.setStatusBarHidden(true, with: UIStatusBarAnimation.none)
}
    
    
override func viewDidLoad() {
        super.viewDidLoad()
    
    // Resize Views accordingly to device screen size
    captureImage.frame = CGRect(x: 0, y: 44, width: self.view.frame.size.width, height: self.view.frame.size.width)
    imagePreviewView.frame = CGRect(x: 0, y: 44, width: self.view.frame.size.width, height: self.view.frame.size.width)
    gridImage.frame = CGRect(x: 0, y: 44, width: self.view.frame.size.width, height: self.view.frame.size.width)
    
    // Rotate exposure slider 90°CW
    exposureSlider.transform = CGAffineTransform(rotationAngle: CGFloat(-M_PI_2))
    
    
    // Set up some Views & Buttons
    FrontCamera = false;
    captureImage.isHidden = false;
    
    // Initialize Camera ====
    initializeCamera()
    
}

   
    
// MARK: - CAMERA INITIALIZATION
func initializeCamera() {
        
        // Remove all inputs and Outputs from the CaptureSession
        captureSession.beginConfiguration()
        captureSession.removeInput(videoInputDevice)
        captureSession.removeOutput(stillImageOutput)
        previewLayer?.removeFromSuperlayer()
        captureSession.commitConfiguration()
        captureSession.stopRunning()
        
        // Init AVCaptureSession
        captureSession.sessionPreset = AVCaptureSessionPresetHigh
        
        // Alloc and set the PreviewLayer for the camera
        previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        previewLayer?.videoGravity = AVLayerVideoGravityResizeAspectFill
        imagePreviewView.layer.addSublayer(previewLayer!)
        previewLayer?.frame = imagePreviewView.bounds
        
        // Get the 2 camera devices (Back & Front)
        let devices = AVCaptureDevice.devices()
        
        // Prepare the Back or Front Camera Devices
        //let device: AVCaptureDevice?
        for device in devices! {
            if (device as AnyObject).hasMediaType(AVMediaTypeVideo) {
                if (device as AnyObject).position == AVCaptureDevicePosition.back {
                    backCameraDev = device as? AVCaptureDevice
                } else {
                    frontCameraDev = device as? AVCaptureDevice
                }
            }
        }
        

    
        // BACK CAMERA Input ---------------------------------------------------
        if !FrontCamera {
            // Check if Back Camera has Flas
            if (backCameraDev?.isFlashModeSupported(AVCaptureFlashMode.on) != nil) {
                print("flashMode: \(backCameraDev!.flashMode.hashValue)")
                
                if backCameraDev?.isFlashAvailable == true {
                    flashOutlet.isEnabled = true
                    do { try backCameraDev?.lockForConfiguration()
                    } catch _ {}
                    
                    if FlashON {
                        backCameraDev?.flashMode = AVCaptureFlashMode.on
                    } else {
                        backCameraDev?.flashMode = AVCaptureFlashMode.off
                        backCameraDev?.unlockForConfiguration()
                    }
                    
                } else {  flashOutlet.isEnabled = false  }
                
                
                // Set Default Exposure
                do { try backCameraDev?.lockForConfiguration() }
                catch _ {}
                
                backCameraDev?.setExposureTargetBias(0.0, completionHandler: nil)
                backCameraDev?.unlockForConfiguration()
                
                do {  videoInputDevice = try AVCaptureDeviceInput(device: backCameraDev)
                } catch { videoInputDevice = nil }
                
                captureSession.addInput(videoInputDevice)
            }
        } // END BACK CAMERA
        
    
    
        
        
    // FRONT CAMERA Input -----------------------------------------
    if FrontCamera {
        flashOutlet.isEnabled = false
            
        do { try frontCameraDev?.lockForConfiguration()
        } catch _ {}
        frontCameraDev?.setExposureTargetBias(0.0, completionHandler: nil)
        frontCameraDev?.unlockForConfiguration()
        
        do { videoInputDevice = try AVCaptureDeviceInput(device: frontCameraDev)
        } catch { videoInputDevice = nil }
        captureSession.addInput(videoInputDevice)
        
    } // END FRONT CAMERA
        
        
        
        // Process the StillImageOutput
        if stillImageOutput != nil {
            stillImageOutput = nil
        }
        stillImageOutput = AVCaptureStillImageOutput()
        let outputSettings: NSDictionary = NSDictionary(object: AVVideoCodecJPEG, forKey: AVVideoCodecKey as NSCopying)
        stillImageOutput?.outputSettings = outputSettings as! [AnyHashable: Any]
        captureSession.addOutput(stillImageOutput)
        
        captureSession.startRunning()
}

    
    
    
    
    
// MARK: - SNAP IMAGE BUTTON
@IBAction func snapPictureButt(_ sender: AnyObject) {
        captureImage.image = nil       //Remove old image from View
        captureImage.isHidden = false    //Show the captured image view
        imagePreviewView.isHidden = true //Hide the live video feed
        
        // Call the method to capture the photo
        capturePhoto()
}
    
    
func capturePhoto()  {
    if stillImageOutput != nil {
            let layer : AVCaptureVideoPreviewLayer = previewLayer! as AVCaptureVideoPreviewLayer
            stillImageOutput?.connection(withMediaType: AVMediaTypeVideo).videoOrientation = layer.connection.videoOrientation
            
            stillImageOutput?.captureStillImageAsynchronously(from: stillImageOutput?.connection(withMediaType: AVMediaTypeVideo), completionHandler: { (imageDataSampleBuffer,error) -> Void in
                
                if ((imageDataSampleBuffer) != nil) {
                    let imageData : Data = AVCaptureStillImageOutput.jpegStillImageNSDataRepresentation(imageDataSampleBuffer)

                    // Show taken photo
                    self.captureImage.image = UIImage(data: imageData)!

                    // Flip the taken photo horizontally if it has been taken from Front Camera
                    if self.FrontCamera {
                        self.captureImage.image = UIImage(cgImage: (self.captureImage.image?.cgImage)!, scale: 1.0, orientation: UIImageOrientation.leftMirrored)
                    }
                    
                    // Stop capture Session
                      self.captureSession.stopRunning()

                      self.cropImage()
                }
                
            })
        }
        flashOutlet.isEnabled = false
}

    
    
    
    
    
// MARK: - CROP IMAGE INTO THE IMAGE PREVIEW VIEW (A SQUARE)
func cropImage() {
    let rect:CGRect = imagePreviewView.frame
    UIGraphicsBeginImageContextWithOptions(rect.size, true, 0.0)
    captureImage.layer.render(in: UIGraphicsGetCurrentContext()!)
    takenImage = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    
    // Get taken image abnd pass it to the ImageEditor
    captureImage.image = takenImage
    
    goToCropImageVC()

}
    


    
    
// MARK: - TOUCH THE IMAGE TO SHOW EXPOSURE VIEW
override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        // Get the location of the finger touch on the screen
        let touch = touches.first
        let currentLocation = touch!.location(in: self.view)
    
    if currentLocation.y <= captureImage.frame.size.height+44 {
        // Show exposureView and its controls
        exposureView.isHidden = false
    }
}
    
// MARK: - TOUCHES MOVED
override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        // Get the location of the finger touch on the screen
        let touch = touches.first
        let currentLocation = touch!.location(in: self.view)
    
        // Make exposureView following your finger
        exposureView.center = currentLocation
}


    
    
    
// MARK: - SWITCH FRONT/BACK CAMERA
@IBAction func switchCameraButt(_ sender: AnyObject) {
        
        captureSession.stopRunning()
        FrontCamera = !FrontCamera
        // println("frontCam: \(FrontCamera)")
        
        // Check multiple cameras (Front and Back)
        if AVCaptureDevice.devices(withMediaType: AVMediaTypeVideo).count > 1  {
            // Remove all inputs and Outputs from the CaptureSession
            captureSession.beginConfiguration()
            captureSession.removeInput(videoInputDevice)
            captureSession.removeOutput(stillImageOutput)
            previewLayer?.removeFromSuperlayer()
            captureSession.commitConfiguration()
            
            // Recall the camera initialization
            DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
                self.initializeCamera()
            })
        }
}

    

    
    
    
// MARK: - FLASH BUTTON
@IBAction func flashButt(_ sender: AnyObject) {
        FlashON = !FlashON
        print("FlashON: \(FlashON)")
        
        // BACK Camera with Flash available
        if !FrontCamera {
            // In case the Back camera has Flash
            if backCameraDev?.isFlashModeSupported(AVCaptureFlashMode.on) != nil {
                do { try backCameraDev?.lockForConfiguration()
                } catch _ {}
                
                if FlashON { // Flash ON
                    backCameraDev?.flashMode = AVCaptureFlashMode.on
                    flashOutlet.setBackgroundImage(UIImage(named: "flashON"), for: .normal)
                } else {  // Flash OFF
                    backCameraDev?.flashMode = AVCaptureFlashMode.off
                    backCameraDev?.unlockForConfiguration()
                    flashOutlet.setBackgroundImage(UIImage(named: "flashOFF"), for: .normal)
                }
                
            }
        }
        
}
    

    
    
    
// MARK: - GRID BUTTON
@IBAction func gridButt(_ sender: AnyObject) {
    gridON = !gridON
    
    if gridON {
        gridImage.isHidden = false
        gridOutlet.setBackgroundImage(UIImage(named: "gridON"), for: .normal)
    } else {
        gridImage.isHidden = true
        gridOutlet.setBackgroundImage(UIImage(named: "gridOFF"), for: .normal)
    }
}

    
    
    
// MARK: - EXPOSURE SLIDER METHOD
@IBAction func exposureChanged(_ sender: UISlider) {
    let slider = sender as UISlider
    
    if !FrontCamera {
        do {try backCameraDev?.lockForConfiguration()
        } catch _ {}
        
        backCameraDev?.setExposureTargetBias(slider.value, completionHandler: nil)
        backCameraDev?.unlockForConfiguration()
        
    } else {
        do { try frontCameraDev?.lockForConfiguration()
        } catch _ {}
        
        frontCameraDev?.setExposureTargetBias(slider.value, completionHandler: nil)
        frontCameraDev?.unlockForConfiguration()
    }
}
    
    
    
// MARK: - DISMISS BUTTON
@IBAction func dismissButt(_ sender: AnyObject) {
    dismiss(animated: true, completion: nil)
}

// MARK: - OPEN IMAGE EDITOR VC
func goToCropImageVC() {
    // Save original taken picture into Photo Library
    if saveImageToLibrary {  UIImageWriteToSavedPhotosAlbum(takenImage!, nil, nil, nil)  }
    
    let ciVC = self.storyboard?.instantiateViewController(withIdentifier: "CropImageVC") as! CropImageVC
    present(ciVC, animated: true, completion: nil)
}
  
    
    
    
    
    
override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

