/*-----------------------------------

- CamFun -

created by FV iMAGINATION © 2015
for CodeCanyon.net

-----------------------------------*/


import UIKit
import Social
import MessageUI
import Parse


var avatarImg: UIImage?



class ShareVC: UIViewController,
MFMailComposeViewControllerDelegate,
UIDocumentInteractionControllerDelegate,
UIAlertViewDelegate
{

    /* Views */
    @IBOutlet var containerView: UIView!
    @IBOutlet weak var imageToBeShared: UIImageView!
    var docIntController = UIDocumentInteractionController()
    
    let hud = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 80, height: 80))

    // Buttons Outlet Collection
    @IBOutlet var buttons: [UIButton]!
    
    
    
    
override var prefersStatusBarHidden : Bool {
    return true
}
override func viewDidLoad() {
        super.viewDidLoad()
    
    // Round buttons corners
    for button in buttons {
        button.layer.cornerRadius = 20
    }
    
    
    // Get the image edited in Image Editor
    imageToBeShared.image = editedImage
    
    // Place all buttons in the center of the screen
    containerView.center = CGPoint(x: view.frame.size.width/2, y: view.frame.size.height/2)
    
    // Get current User (if logged in)
    if PFUser.current() != nil {
        let currentUser = PFUser.current()!
        let avatarImage = currentUser[USER_AVATAR] as! PFFile
        avatarImage.getDataInBackground(block: { (imageData, error) in
            if error == nil {
                if let imageData = imageData {
                    avatarImg =  UIImage(data:imageData)
        }}})
    }
    
}

    
// MARK: - DISMISS CONTROLLER
@IBAction func dismissButt(_ sender: AnyObject) {
    self.dismiss(animated: true, completion: nil)
}

    
 
// MARK: - SAVE TO PHOTO LIBRARY
@IBAction func saveToLibraryButt(_ sender: AnyObject) {
    UIImageWriteToSavedPhotosAlbum(imageToBeShared.image!, nil,nil, nil)
    simpleAlert("Your picture has been saved to Photo Library!")
}
    
    
    
    
// MARK: - POST TO WALL OF FAME BUTTON
@IBAction func postToWOFbutt(_ sender: AnyObject) {
    showHUD()
    Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(postToWOF), userInfo: nil, repeats: false)
}
    
func postToWOF() {

    // YOU'RE LOGGED IN -> CAN POST IMAGE TO WALL OF FAME
    if PFUser.current() != nil {
    
        // Save your image to "Posts" Class in Parse
        let posts = PFObject(className: POSTS_CLASS_NAME)
        let imageData = UIImageJPEGRepresentation(imageToBeShared.image!, 0.5)
        let imageFile = PFFile(name:"image.jpg", data:imageData!)
        posts[POSTS_IMAGE_FILE] = imageFile
    
        // Upload current User username and Pointer
        posts[POSTS_USER] = PFUser.current()!
        posts[POSTS_USERNAME] = PFUser.current()!.username
       
        // Execure Parse query
        posts.saveInBackground(block: { (succ, error) in
            if error == nil {
                self.hideHUD()

                let alert = UIAlertView(title: APP_NAME,
                    message: "Your Picture has been posted in the Wall Of Fame!\nWant to check it out?",
                    delegate: self,
                    cancelButtonTitle: "Not now",
                    otherButtonTitles: "Yes!" )
                alert.show()
            } else {
                self.simpleAlert("\(error!.localizedDescription)")
                self.hideHUD()
        }})
        

        
        
    // YOU MUST LOGIN OR CREATE AN ACCOUNT TO POST ON WALL OF FAME
    } else {
        self.simpleAlert("You need to login to post on Wall Of Fame.\nGo to Settings and Login into your Profile, or create a new account!")
        self.hideHUD()
    }

}
    
// AlertView delegate
func alertView(_ alertView: UIAlertView, clickedButtonAt buttonIndex: Int) {
    if alertView.buttonTitle(at: buttonIndex) == "Yes!" {
        let wofVC = self.storyboard?.instantiateViewController(withIdentifier: "WallOfFameVC") as! WallOfFameVC
        self.present(wofVC, animated: true, completion: nil)
    }
}
 
    
    
    
// MARK: - TWITTER SHARING BUTTON
@IBAction func twitterButt(_ sender: AnyObject) {
    if SLComposeViewController.isAvailable(forServiceType: SLServiceTypeTwitter) {
            let twSheet = SLComposeViewController(forServiceType: SLServiceTypeTwitter)
            twSheet?.setInitialText(SHARING_MESSAGE)
            twSheet?.add(imageToBeShared.image)
            self.present(twSheet!, animated: true, completion: nil)
        } else {
        let alert: UIAlertView = UIAlertView(title: "Twitter",
            message: "Please login to your Twitter account in Settings", delegate: self,
            cancelButtonTitle: "OK")
        alert.show()
    }
}
  
    
    
// MARK: - FACEBOOK SHARING BUTTON
@IBAction func facebookButt(_ sender: AnyObject) {
    if SLComposeViewController.isAvailable(forServiceType: SLServiceTypeFacebook) {
            let fbSheet = SLComposeViewController(forServiceType: SLServiceTypeFacebook)
            fbSheet?.setInitialText(SHARING_MESSAGE)
            fbSheet?.add(imageToBeShared.image)
            self.present(fbSheet!, animated: true, completion: nil)
    } else {
        let alert: UIAlertView = UIAlertView(title: "Facebook",
            message: "Please login to your Facebook account in Settings", delegate: self,
            cancelButtonTitle: "OK")
            alert.show()
        }
}
    
    
    
    
    
// MARK: -  MAIL SHARING BUTTON
@IBAction func mailButt(_ sender: AnyObject) {
    let mailComposer = MFMailComposeViewController()
    mailComposer.mailComposeDelegate = self
    mailComposer.setSubject(SHARING_SUBJECT)
    mailComposer.setMessageBody(SHARING_MESSAGE, isHTML: true)
    
    // Attach image
    let imageData = UIImageJPEGRepresentation(imageToBeShared.image!, 1.0)
    mailComposer.addAttachmentData(imageData!, mimeType: "image/png", fileName: "MyPhoto.png")
    
    if MFMailComposeViewController.canSendMail() {
        self.present(mailComposer, animated: true, completion: nil)
    }
}
// Email delegate
func mailComposeController(_ controller:MFMailComposeViewController, didFinishWith result:MFMailComposeResult, error:Error?) {
    var outputMessage = ""
    switch result.rawValue {
        case MFMailComposeResult.cancelled.rawValue:
            outputMessage = "Mail cancelled"
        case MFMailComposeResult.saved.rawValue:
            outputMessage = "Mail saved"
        case MFMailComposeResult.sent.rawValue:
            outputMessage = "Mail sent"
        case MFMailComposeResult.failed.rawValue:
            outputMessage = "Something went wrong with sending Mail, try again later."
        default: break
    }
    
    let alert = UIAlertView(title: APP_NAME,
    message: outputMessage,
    delegate: self,
    cancelButtonTitle: "OK" )
    alert.show()

    dismiss(animated: false, completion: nil)
}
    
    
 
// MARK: - OTHER APPS BUTTON
@IBAction func otherAppsButt(_ sender: AnyObject) {
    // NOTE: The following method works only on a Real device, not on iOS Simulator, + You should have apps like Instagram, iPhoto, etc. already installed into your device!
    
    //Save the Image to default device Directory
    let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
    let savedImagePath:String = paths + "/image.jpg"
    let imageData: Data = UIImageJPEGRepresentation(imageToBeShared.image!, 1.0)!
    try? imageData.write(to: URL(fileURLWithPath: savedImagePath), options: [])
    
    //Load the Image Path
    let getImagePath = paths + "/image.jpg"
    let fileURL: URL = URL(fileURLWithPath: getImagePath)
    
    // Open the Document Interaction controller for Sharing options
    docIntController.delegate = self
    docIntController = UIDocumentInteractionController(url: fileURL)
    docIntController.presentOpenInMenu(from: CGRect.zero, in: self.view, animated: true)
}
    
    
 
    
// MARK: - INSTAGRAM SHARING
@IBAction func instagramButt(_ sender: AnyObject) {
    let instagramURL = URL(string: "instagram://app")!
    if UIApplication.shared.canOpenURL(instagramURL) {
        
        //Save the Image to default device Directory
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        let savedImagePath:String = paths + "/image.igo"
        let imageData: Data = UIImageJPEGRepresentation(imageToBeShared.image!, 1.0)!
        try? imageData.write(to: URL(fileURLWithPath: savedImagePath), options: [])
        
        //Load the Image Path
        let getImagePath = paths + "/image.igo"
        let fileURL: URL = URL(fileURLWithPath: getImagePath)
        
        
        docIntController = UIDocumentInteractionController(url: fileURL)
        docIntController.uti = "com.instagram.exclusivegram"
        docIntController.delegate = self
        docIntController.presentOpenInMenu(from: CGRect.zero, in: self.view, animated: true)
        
    } else {
        let alert:UIAlertView = UIAlertView(title: APP_NAME,
            message: "Instagram not found, please download it on the App Store",
            delegate: nil,
            cancelButtonTitle: "OK")
        alert.show()
    }
    

}


    
// MARK: - WHATSAPP SHARING
@IBAction func whatsAppButt(_ sender: AnyObject) {
    // NOTE: The following method works only on a Real device, not on iOS Simulator
    
    let whatsappURL = URL(string: "whatsapp://app")!
    if UIApplication.shared.canOpenURL(whatsappURL) {
        
        //Save the Image to default device Directory
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        let savedImagePath:String = paths + "/image.wai"
        let imageData: Data = UIImageJPEGRepresentation(imageToBeShared.image!, 1.0)!
        try? imageData.write(to: URL(fileURLWithPath: savedImagePath), options: [])
        
        //Load the Image Path
        let getImagePath = paths + "/image.wai"
        let fileURL: URL = URL(fileURLWithPath: getImagePath)
        
        // Open the Document Interaction controller for Sharing options
        docIntController.delegate = self
        docIntController.uti = "net.whatsapp.image"
        docIntController = UIDocumentInteractionController(url: fileURL)
        docIntController.presentOpenInMenu(from: CGRect.zero, in: self.view, animated: true)
        
    } else {
        let alert = UIAlertView(title: APP_NAME,
            message: "WhatsApp not found. Please install it on your device",
            delegate: nil,
            cancelButtonTitle: "OK")
        alert.show()
    }
}
    
    
override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
}
}
