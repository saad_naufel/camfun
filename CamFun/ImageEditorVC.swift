/*-----------------------------------

- CamFun -

created by FV iMAGINATION © 2015
for CodeCanyon.net

-----------------------------------*/


import UIKit
import GoogleMobileAds
import AudioToolbox


class ImageEditorVC: UIViewController,
UIAlertViewDelegate,
GADBannerViewDelegate
{
    
    /* Views */
    @IBOutlet var containerView: UIView!
    @IBOutlet var originalImage: UIImageView!
    
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var watermarkLabel: UILabel!
    
    //Ad banners properties
    let adMobBannerView = GADBannerView()
    
    

    
    
override var prefersStatusBarHidden : Bool {
        return true
}
override func viewDidLoad() {
        super.viewDidLoad()
    
    // Set controller's main title
    titleLabel.text = "Image Editor"
    
    // Get the cropped image previously made
    originalImage.image = croppedImage
    
    // Set the image for filters as the Original image
    imageForFilters.image = originalImage.image
    
    
    // Resize containerView accordingly to the screen size
    if UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.phone {
        // iPhone
        containerView.frame = CGRect(x: 0, y: 44, width: view.frame.size.width, height: view.frame.size.width)
    } else  {
        // iPad
        containerView.frame = CGRect(x: 84, y: 50, width: 600, height: 600)
    }
    
    // Place Tool Views outside the screen, on the bottom
    framesToolView.frame.origin.y = view.frame.size.height
    stickersToolView.frame.origin.y = view.frame.size.height
    texturesToolView.frame.origin.y = view.frame.size.height
    filtersToolView.frame.origin.y = view.frame.size.height
    adjustmentToolView.frame.origin.y = view.frame.size.height
    
    
    // Call all Tools Setup methods once
    setupFramesTool()
    setupStickersTool()
    setupTexturesTool()
    setupFiltersTool()
    
    
    // Init ad banners
    initAdMobBanner()
    
}

    
    

    
/*======================================================================
========================================================================
    FRAMES TOOL
========================================================================
========================================================================*/
    

    /* Views */
    @IBOutlet var framesToolView: UIView!
    @IBOutlet var framesColorsScrollView: UIScrollView!
    @IBOutlet var framesSlider: UISlider!
    @IBOutlet var framesScrollView: UIScrollView!
    var frameButt: UIButton?
    var frameColorButt: UIButton?
    var frameImage: UIImageView?

    
// MARK: - FRAMES BUTTON
@IBAction func framesButt(_ sender: AnyObject) {
    showToolView(FRAMESTOOL_NAME)
    view.bringSubview(toFront: framesToolView)
}
    
func setupFramesTool() {
    var xCoord: CGFloat = 0
    let yCoord: CGFloat = 3
    let buttonWidth:CGFloat = framesScrollView.frame.size.height-6
    let buttonHeight: CGFloat = framesScrollView.frame.size.height-6
    let gapBetweenButtons: CGFloat = 10
    
    // Loop for creating buttons =========================
    var itemCount = 0
    for i in 0..<10+1 {
        itemCount = i
        // Create a Button for each Frame ==========
        frameButt = UIButton(type: .custom)
        frameButt?.frame = CGRect(x: xCoord, y: yCoord, width: buttonWidth, height: buttonHeight)
        frameButt?.tag = i
        frameButt?.showsTouchWhenHighlighted = true
        frameButt?.setImage(UIImage(named: "f\(itemCount)"), for: .normal)
        frameButt?.setTitleColor(UIColor.white, for: .normal)
        frameButt?.addTarget(self, action: #selector(frameButtTapped(_:)), for: .touchUpInside)
        frameButt?.layer.cornerRadius = frameButt!.bounds.size.width/2
        frameButt?.clipsToBounds = true

        // Add Buttons based on xCood
        xCoord +=  buttonWidth + gapBetweenButtons
        framesScrollView.addSubview(frameButt!)
        } // END LOOP =========================================
    
    
    framesScrollView.contentSize = CGSize(width: (buttonWidth+gapBetweenButtons) * CGFloat(itemCount+2), height: yCoord)

    setupFramesColorsMenu()
}
    
func frameButtTapped(_ sender: AnyObject) {
    let butt = sender as! UIButton
    
    if (frameImage != nil) {
        frameImage?.removeFromSuperview()
    }
        
    frameImage = UIImageView(frame: CGRect(x: 0, y: 0, width: containerView.frame.size.width, height: containerView.frame.size.height))
    frameImage?.backgroundColor = UIColor.clear
    frameImage?.image = UIImage(named: "f\(butt.tag)")
    frameImage?.isUserInteractionEnabled = true
    frameImage?.isMultipleTouchEnabled = true
    frameImage?.alpha = CGFloat(framesSlider.value)
    containerView.addSubview(frameImage!)
    
    // Remove Frame when empty button is selected
    if sender.tag == 0 {
        frameImage?.removeFromSuperview()
    }
            
    // Add PINCH GESTURES to the selected Frame
    let pinchGestBorder = UIPinchGestureRecognizer()
    pinchGestBorder.addTarget(self, action: #selector(zoomFrame(_:)))
    frameImage!.addGestureRecognizer(pinchGestBorder)
    
    
    if stickerImage != nil {
        for stkrCount in 0..<stickersTAGArray.count {
            let viewTAG = stickersTAGArray[stkrCount]
            containerView.bringSubview(toFront: view.viewWithTag(viewTAG)! )
        }
    }
    
    // Bring CamFun watermark to FRONT
    containerView.bringSubview(toFront: watermarkLabel)
}

    
// MARK: - ZOOM FRAME
func zoomFrame(_ sender: UIPinchGestureRecognizer) {
        if sender.state == .ended ||
        sender.state == .changed  {
            let currentScale: CGFloat = CGFloat(frameImage!.frame.size.width / frameImage!.bounds.size.width)
            var newScale: CGFloat = CGFloat(currentScale * sender.scale)
                
            //Constrain zoom to specific scale ratios
            if newScale < 1.0 {   newScale = 1.0   }
            if newScale > 2.0 {   newScale = 2.0   }
            let transform = CGAffineTransform(scaleX: newScale, y: newScale)
            frameImage!.transform = transform
            sender.scale = 1
    }
}
    
    
// MARK: - SETUP COLORS FOR FRAMES
func setupFramesColorsMenu() {
    var xCoord: CGFloat = 0
    let yCoord: CGFloat = 0
    let buttonWidth:CGFloat = framesColorsScrollView.frame.size.height
    let buttonHeight: CGFloat = framesColorsScrollView.frame.size.height
    let gapBetweenButtons: CGFloat = 0
        
        
    // Loop for creating buttons ========
    var colorsCount = 0

    for i in 0..<colorsList.count {
            colorsCount = i
            // Create a Button for each Color ==========
            frameColorButt = UIButton(type: .custom)
            frameColorButt?.frame = CGRect(x: xCoord, y: yCoord, width: buttonWidth, height: buttonHeight)
            frameColorButt?.tag = colorsCount
            frameColorButt?.backgroundColor = colorsList[colorsCount]
            frameColorButt?.showsTouchWhenHighlighted = true
            frameColorButt?.addTarget(self, action: #selector(frameColorButtTapped(_:)), for: .touchUpInside)
            
            xCoord +=  buttonWidth + gapBetweenButtons
            framesColorsScrollView.addSubview(frameColorButt!)
        } // END LOOP ================================
        
        
        framesColorsScrollView.contentSize = CGSize( width: (buttonWidth+gapBetweenButtons) * CGFloat(colorsCount+2), height: yCoord)
}
func frameColorButtTapped(_ sender: AnyObject) {
    frameImage?.image = frameImage?.image?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
    frameImage?.tintColor = sender.backgroundColor
}
    
    
// MARK: - FRAMES SLIDER METHOD
@IBAction func frameAlphaChanged(_ sender: UISlider) {
    frameImage?.alpha = CGFloat(sender.value)
}
    

    
// MARK: - CLOSE FRAMES TOOL BUTTON
@IBAction func framesCloseButt(_ sender: AnyObject) {
        hideToolView(FRAMESTOOL_NAME)
}
/*========================================================================*/
/*  END FRAMES TOOL ============================*/
    
    
    
   
    
    
    
    
   
/*======================================================================
========================================================================
    STICKERS
    TOOL
========================================================================
========================================================================*/
    
    /* Views */
    @IBOutlet weak var stickersToolView: UIView!
    @IBOutlet weak var stickersScrollView: UIScrollView!
    var stickerButt: UIButton?
    var stickerImage: UIImageView?
    
    /* Variables */
    var stickersTAGArray = [Int]()
    var sndrTAG = 0
    
    
    
// MARK: - STICKERS BUTTON
@IBAction func stickersButt(_ sender: AnyObject) {
    showToolView(STICKERSTOOL_NAME)
    view.bringSubview(toFront: stickersToolView)
}
    
func setupStickersTool() {
        var xCoord: CGFloat = 0
        let yCoord: CGFloat = 3
        let buttonWidth:CGFloat = stickersScrollView.frame.size.height-6
        let buttonHeight: CGFloat = stickersScrollView.frame.size.height-6
        let gapBetweenButtons: CGFloat = 10
    
    
    // Loop for creating buttons =========================
    var itemCount = 0
    for i in 0..<10+1 {
        itemCount = i
        
        stickerButt = UIButton(type: .custom)
        stickerButt?.frame = CGRect(x: xCoord, y: yCoord, width: buttonWidth, height: buttonHeight)
        stickerButt?.tag = itemCount
        stickerButt?.showsTouchWhenHighlighted = true
        stickerButt?.setImage(UIImage(named: "s\(itemCount)"), for: .normal)
        stickerButt?.setTitleColor(UIColor.white, for: .normal)
        stickerButt?.addTarget(self, action: #selector(stickerButtTapped(_:)), for: .touchUpInside)
        stickerButt?.layer.cornerRadius = stickerButt!.bounds.size.width/2
        stickerButt?.clipsToBounds = true
        
        // Add Buttons & Labels based on xCood
        xCoord +=  buttonWidth + gapBetweenButtons
        stickersScrollView.addSubview(stickerButt!)
    } // END LOOP =========================================
        
        
    // Place Buttons into the ScrollView =====
    stickersScrollView.contentSize = CGSize(width: (buttonWidth+gapBetweenButtons) * CGFloat(itemCount+2), height: yCoord)

}
    
    
func stickerButtTapped(_ sender: AnyObject) {
    let butt = sender as! UIButton
    
        sndrTAG = 100
        print("sndrTAG: \(sndrTAG)")
        
        stickerImage = UIImageView(frame: CGRect(x: 0, y: 0, width: 200, height: 200))
        stickerImage?.center = CGPoint(x: containerView.frame.size.width/2, y: containerView.frame.size.height/2)
        stickerImage?.backgroundColor = UIColor.clear
        stickerImage?.image = UIImage(named: "s\(butt.tag)")
        stickerImage?.tag = sndrTAG
        stickerImage?.isUserInteractionEnabled = true
        stickerImage?.isMultipleTouchEnabled = true
        containerView.addSubview(stickerImage!)
        
        // Add an array of stickers
        stickersTAGArray.append(stickerImage!.tag)
        print("stickersTAGArray:\(stickersTAGArray)")
        
        
        // Add PAN GESTURES to the sticker
        let panGestSticker = UIPanGestureRecognizer()
        panGestSticker.addTarget(self, action: #selector(moveSticker(_:)))
        stickerImage!.addGestureRecognizer(panGestSticker)
        
        // Add DOUBLE TAP GESTURES to the sticker
        let doubletapGestSticker = UITapGestureRecognizer()
        doubletapGestSticker.addTarget(self, action: #selector(deleteSticker(_:)))
        doubletapGestSticker.numberOfTapsRequired = 2
        stickerImage!.addGestureRecognizer(doubletapGestSticker)
        
        // Add PINCH GESTURES to the sticker
        let pinchGestSticker = UIPinchGestureRecognizer()
        pinchGestSticker.addTarget(self, action: #selector(zoomSticker(_:)))
        stickerImage!.addGestureRecognizer(pinchGestSticker)
        
        
        // Add ROTATION GESTURES to the sticker
        let rotationGestSticker = UIRotationGestureRecognizer()
        rotationGestSticker.addTarget(self, action: #selector(rotateSticker(_:)))
        stickerImage!.addGestureRecognizer(rotationGestSticker)
}
    
    
// MOVE STICKER
func moveSticker(_ sender: UIPanGestureRecognizer) {
        let translation: CGPoint =  sender.translation(in: self.view)
        sender.view?.center = CGPoint(x: sender.view!.center.x +  translation.x, y: sender.view!.center.y + translation.y)
        sender.setTranslation(CGPoint(x: 0, y: 0), in: self.view)
}
// DELETE STICKER (with a double-tap on a sticker
func deleteSticker(_ sender: UITapGestureRecognizer) {
        sender.view?.removeFromSuperview()
        stickersTAGArray.removeLast()
        //println("stkrTAGArray2:\(stickersTAGArray)")
}
// ZOOM STICKER
func zoomSticker(_ sender: UIPinchGestureRecognizer) {
        sender.view?.transform = sender.view!.transform.scaledBy(x: sender.scale, y: sender.scale)
        sender.scale = 1
}
// ROTATE STICKER
func rotateSticker(_ sender:UIRotationGestureRecognizer){
    if sender.state == UIGestureRecognizerState.began ||
    sender.state == UIGestureRecognizerState.changed  {
            sender.view!.transform = sender.view!.transform.rotated(by: sender.rotation)
            sender.rotation = 0
    }
}
    
    
    
@IBAction func closeStickersButt(_ sender: AnyObject) {
    hideToolView(STICKERSTOOL_NAME)
}

/*========================================================================
     END STICKERS TOOL ============================*/
    

    
    
    
    
    
  
/*======================================================================
========================================================================
    TEXTURES
    TOOL
========================================================================
========================================================================*/
    
    
    /* Views */
    @IBOutlet weak var texturesToolView: UIView!
    @IBOutlet weak var texturesScrollView: UIScrollView!
    var textureButt: UIButton?
    @IBOutlet weak var textureSlider: UISlider!

    var textureImage: UIImageView?

    
    
// MARK: - TEXTURES BUTTON
@IBAction func texturesButt(_ sender: AnyObject) {
    showToolView(TEXTURESTOOL_NAME)
    view.bringSubview(toFront: texturesToolView)
}

    
func setupTexturesTool() {
    // Variables for setting the Buttons
    var xCoord: CGFloat = 0
    let yCoord: CGFloat = 3
    let buttonWidth:CGFloat = texturesScrollView.frame.size.height-6
    let buttonHeight: CGFloat = texturesScrollView.frame.size.height-6
    let gapBetweenButtons: CGFloat = 10
    
    
    // Loop for creating buttons =========================
    var itemCount = 0
    
    for i in 0..<9+1 {
        itemCount = i
  
        // Create a Button for each Texture ==========
        textureButt = UIButton(type: .custom)
        textureButt?.frame = CGRect(x: xCoord, y: yCoord, width: buttonWidth, height: buttonHeight)
        textureButt?.tag = itemCount
        textureButt?.showsTouchWhenHighlighted = true
        textureButt?.setImage(UIImage(named: "t\(itemCount)"), for: .normal)
        textureButt?.setTitleColor(UIColor.white, for: .normal)
        textureButt?.addTarget(self, action: #selector(textureButtTapped(_:)), for: .touchUpInside)
        textureButt?.layer.cornerRadius = textureButt!.bounds.size.width/2
        textureButt?.clipsToBounds = true
            
            
        // Add Buttons & Labels based on xCood
        xCoord +=  buttonWidth + gapBetweenButtons
        texturesScrollView.addSubview(textureButt!)
    } // END LOOP =========================================
        
    texturesScrollView.contentSize = CGSize( width: (buttonWidth + gapBetweenButtons) * CGFloat(itemCount+2), height: yCoord)
        
}
    
    
func textureButtTapped(_ sender: AnyObject) {
    let butt = sender as! UIButton
    
    if (textureImage != nil) {   textureImage?.removeFromSuperview()  }
        
    textureImage = UIImageView(frame: CGRect(x: 0, y: 0, width: containerView.frame.size.width, height: containerView.frame.size.height))
    textureImage?.center = CGPoint(x: containerView.frame.size.width/2, y: containerView.frame.size.height/2)
    textureImage?.backgroundColor = UIColor.clear
    textureImage?.image = UIImage(named: "t\(butt.tag)")
    textureImage?.isUserInteractionEnabled = true
    textureImage?.isMultipleTouchEnabled = true
    textureImage?.alpha = CGFloat(textureSlider.value)
    containerView.addSubview(textureImage!)
        
    if sender.tag == 0 {
        textureImage?.removeFromSuperview()
    }
        
        
    // Bring Frames and Stickers to FRONT ============
    if frameImage != nil {
        containerView.bringSubview(toFront: frameImage!)
    }

    if stickerImage != nil {
        for stkrCount in 0..<stickersTAGArray.count {
            let viewTAG = stickersTAGArray[stkrCount]
            containerView.bringSubview(toFront: self.view.viewWithTag(viewTAG)! )
        }
    }
        
        // Bring watermark to FRONT
        containerView.bringSubview(toFront: watermarkLabel)
    }
    
    
// Texture opacity Slider
@IBAction func textureAlphaChanged(_ sender: UISlider) {
    textureImage?.alpha = CGFloat(sender.value)
}
    
// Rotate Texture Button
@IBAction func rotateTextureButt(_ sender: AnyObject) {
    if textureImage == nil {
        return
    }
    textureImage?.transform = textureImage!.transform.rotated(by: CGFloat (M_PI_2))
}
    

    
    
@IBAction func closeTexturesButt(_ sender: AnyObject) {
    hideToolView(TEXTURESTOOL_NAME)
}    
/*========================================================================
    END TEXTURES TOOL ============================*/
    

    
    
    
    
    
    
   
   
/*======================================================================
========================================================================
    FILTERS
    TOOL
========================================================================
========================================================================*/
    
    /* Views */
    @IBOutlet weak var imageForFilters: UIImageView!
    
    @IBOutlet weak var filtersToolView: UIView!
    @IBOutlet weak var filtersScrollView: UIScrollView!
    var filterButt: UIButton?
    
    
    
// MARK: - FILTERS BUTTON
@IBAction func filtersButt(_ sender: AnyObject) {
    showToolView(FILTERSTOOL_NAME)
    view.bringSubview(toFront: filtersToolView)
}
    
    // Filters List & Names ------------------------------
    let filtersList = [
        "CIVignette",              //0
        "CIVignette",              //1
        "CIPhotoEffectInstant",    //2
        "CIPhotoEffectProcess",    //3
        "CIPhotoEffectTransfer",   //4
        "CISepiaTone",             //5
        "CIPhotoEffectChrome",     //6
        "CIPhotoEffectFade",       //7
        "CIPhotoEffectTonal",      //8
        "CIPhotoEffectNoir",       //9
        "CIDotScreen",             //10
        "CIColorPosterize",        //11
        "CISharpenLuminance",      //12
        "CIGammaAdjust",           //13
        
        // Add here new CIFilters...
    ]
    
    /*-----------------------------------------------*/
    
    
    
    
func setupFiltersTool() {
        // Variables for setting the Buttons
        var xCoord: CGFloat = 0
        let yCoord: CGFloat = 3
        let buttonWidth:CGFloat = filtersScrollView.frame.size.height-6
        let buttonHeight: CGFloat = filtersScrollView.frame.size.height-6
        let gapBetweenButtons: CGFloat = 10
        
        // Loop for creating buttons =========================
        var itemCount = 0
        for i in 0..<filtersList.count {
            itemCount = i

            // Create a Button for each Texture ==========
            filterButt = UIButton(type: .custom)
            filterButt?.frame = CGRect(x: xCoord, y: yCoord, width: buttonWidth, height: buttonHeight)
            filterButt?.tag = itemCount
            filterButt?.showsTouchWhenHighlighted = true
            filterButt?.addTarget(self, action: #selector(filterButtTapped(_:)), for: .touchUpInside)
            filterButt?.layer.cornerRadius = filterButt!.bounds.width/2
            filterButt?.clipsToBounds = true
            
            
        /*=================================================
        ================ FILTER SETTINGS ==================*/
            
            let ciContext = CIContext(options: nil)
            let coreImage = CIImage(image: originalImage.image!)
            let filter = CIFilter(name: "\(filtersList[itemCount])" )
            filter!.setDefaults()
            
            
            switch itemCount {
            case 0: // NO filter (don't edit this filter)
                break
                
            case 1: // Vignette
                filter!.setValue(3.0, forKey: kCIInputRadiusKey)
                filter!.setValue(4.0, forKey: kCIInputIntensityKey)
                break
                
            case 11: //Poster
                filter!.setValue(6.0, forKey: "inputLevels")
                break
                
            case 12: // Sharpen
                filter!.setValue(0.9, forKey: kCIInputSharpnessKey)
                break
                
            case 13: // Gamma Adjust
                filter!.setValue(3, forKey: "inputPower")
                break

                
            /* You can ddd new filters here,
               Check Core Image Filter Reference here: https://developer.apple.com/library/mac/documentation/GraphicsImaging/Reference/CoreImageFilterReference/index.html
            */
                
                
            default: break
        } // END FILTER SETTINGS =========================================
            
            
            
            filter!.setValue(coreImage, forKey: kCIInputImageKey)
            let filteredImageData = filter!.value(forKey: kCIOutputImageKey) as! CIImage
            let filteredImageRef = ciContext.createCGImage(filteredImageData, from: filteredImageData.extent)
            let imageForButton = UIImage(cgImage: filteredImageRef!);
            
            // Assign filtered image to the button
            filterButt!.setBackgroundImage(imageForButton, for: UIControlState())

            // Add Buttons based on xCood
            xCoord +=  buttonWidth + gapBetweenButtons
            filtersScrollView.addSubview(filterButt!)
        } // END LOOP =========================================
        
        
        // Place Buttons into the ScrollView =====
        filtersScrollView.contentSize = CGSize( width: (buttonWidth+gapBetweenButtons) * CGFloat(itemCount+2), height: yCoord)
}
    
func filterButtTapped(_ button: UIButton) {
    // Remove adjImage (if any)
    adjImage?.image = nil
        
    // Reset the Adjustment Tools
    brightnessSlider.value = 0
    contrastSlider.value = 1
    saturationSlider.value = 1
    exposureSlider.value = 0.5

    
    // Set the filteredImage
    imageForFilters.image = button.backgroundImage(for: .normal)
    
    // NO Filter (go back to Original image)
    if button.tag == 0 {   imageForFilters.image = originalImage.image  }
}
    
    
    
    
// CLOSE FILTERS BUTTON ================================
@IBAction func closeFiltersButt(_ sender: AnyObject) {
    hideToolView(FILTERSTOOL_NAME)
}
/*========================================================================
    END FILTERS TOOL ============================*/
    
    

    
    
    
    
    
   
/*======================================================================
    ========================================================================
    ADJUSTMENT
    TOOL
    ========================================================================
    ========================================================================*/
    
    /* Views */
    @IBOutlet weak var adjustmentToolView: UIView!
    
    /* Buttons */
    @IBOutlet weak var brightnessOutlet: UIButton!
    @IBOutlet weak var contrastOutlet: UIButton!
    @IBOutlet weak var saturationOutlet: UIButton!
    @IBOutlet weak var exposureOutlet: UIButton!
    
    /* Sliders */
    @IBOutlet weak var brightnessSlider: UISlider!
    @IBOutlet weak var contrastSlider: UISlider!
    @IBOutlet weak var saturationSlider: UISlider!
    @IBOutlet weak var exposureSlider: UISlider!
    
    /* Adjustment Image */
    var adjImage: UIImageView?
    

    
// MARK: - ADJUSTMENT BUTTON
@IBAction func adjustmentButt(_ sender: AnyObject) {
    showToolView(ADJUSTMENTTOOL_NAME)
    view.bringSubview(toFront: adjustmentToolView)

    brightnessOutlet.layer.cornerRadius = brightnessOutlet.bounds.width/2
    contrastOutlet.layer.cornerRadius = contrastOutlet.bounds.width/2
    saturationOutlet.layer.cornerRadius = saturationOutlet.bounds.width/2
    exposureOutlet.layer.cornerRadius = exposureOutlet.bounds.width/2
}
    
    
// ADJUSTMENT BUTTONS ===================================
@IBAction func adjustmentButtons(_ sender: AnyObject) {
    
        switch (sender.tag) {
        case 0: // Brightness
            titleLabel.text = "Brightness"
            
            brightnessOutlet.backgroundColor = lightGreen
            contrastOutlet.backgroundColor = UIColor.black
            saturationOutlet.backgroundColor = UIColor.black
            exposureOutlet.backgroundColor = UIColor.black
            
            brightnessSlider.isHidden = false
            contrastSlider.isHidden = true
            saturationSlider.isHidden = true
            exposureSlider.isHidden = true
            break
        case 1: // Contrast
            titleLabel.text = "Contrast"
            
            brightnessOutlet.backgroundColor = UIColor.black
            contrastOutlet.backgroundColor = lightGreen
            saturationOutlet.backgroundColor = UIColor.black
            exposureOutlet.backgroundColor = UIColor.black
            
            brightnessSlider.isHidden = true
            contrastSlider.isHidden = false
            saturationSlider.isHidden = true
            exposureSlider.isHidden = true
            break
        case 2: // Saturation
            titleLabel.text = "Saturation"
            
            brightnessOutlet.backgroundColor = UIColor.black
            contrastOutlet.backgroundColor = UIColor.black
            saturationOutlet.backgroundColor = lightGreen
            exposureOutlet.backgroundColor = UIColor.black
            
            brightnessSlider.isHidden = true
            contrastSlider.isHidden = true
            saturationSlider.isHidden = false
            exposureSlider.isHidden = true
            break
        case 3: // Exposure
            titleLabel.text = "Exposure"
            
            brightnessOutlet.backgroundColor = UIColor.black
            contrastOutlet.backgroundColor = UIColor.black
            saturationOutlet.backgroundColor = UIColor.black
            exposureOutlet.backgroundColor = lightGreen
            
            brightnessSlider.isHidden = true
            contrastSlider.isHidden = true
            saturationSlider.isHidden = true
            exposureSlider.isHidden = false
            break
            
        default: break
        }
    
    // CONSOLE LOGS ==================================
    print("brightn: \(brightnessSlider.value)")
    print("contrast: \(contrastSlider.value)")
    print("saturat: \(saturationSlider.value)")
    print("exposure: \(exposureSlider.value)")
}
    
    
// ADJUSTMENT SLIDERS METHOD =====================
@IBAction func adjustmentChanged(_ sender: UISlider) {

    if adjImage != nil {  adjImage?.removeFromSuperview()  }
        
    adjImage = UIImageView(frame: CGRect(x: imageForFilters.frame.origin.x, y: imageForFilters.frame.origin.y, width: imageForFilters.frame.size.width, height: imageForFilters.frame.size.height))
    adjImage?.image = imageForFilters.image
    
    
    // Image process =======================
    let ciContext = CIContext(options: nil)
    let coreImage = CIImage(image: adjImage!.image!)
    let filter = CIFilter(name: "CIColorControls")
    filter!.setDefaults()
        
    filter!.setValue(brightnessSlider.value, forKey: "inputBrightness") // -1 to 1
    filter!.setValue(contrastSlider.value, forKey: "inputContrast")     // 0 to 2
    filter!.setValue(saturationSlider.value, forKey: "inputSaturation") // 0 to 2
        
    let exposureFilter = CIFilter(name: "CIExposureAdjust")
        exposureFilter!.setDefaults()
        exposureFilter!.setValue(exposureSlider.value, forKey:"inputEV")
    
    // Finalize the filtered image ==========
    filter!.setValue(coreImage, forKey: kCIInputImageKey)
    exposureFilter!.setValue(filter!.outputImage, forKey: kCIInputImageKey)
    print("exp: \(exposureFilter!)")

    let filteredImageData = exposureFilter!.value(forKey: kCIOutputImageKey) as! CIImage
    let filteredImageRef = ciContext.createCGImage(filteredImageData, from: filteredImageData.extent)
    adjImage?.image = UIImage(cgImage: filteredImageRef!);
        
    containerView.addSubview(adjImage!)
        
    
    
    // Bring Textures, Frames and Stickers to FRONT ============
    if textureImage != nil {  containerView.bringSubview(toFront: textureImage!)  }
    if frameImage != nil {  containerView.bringSubview(toFront: frameImage!)  }
    if stickerImage != nil {
        for stkrCount in 0..<stickersTAGArray.count {
            let viewTAG = stickersTAGArray[stkrCount]
            containerView.bringSubview(toFront: self.view.viewWithTag(viewTAG)! )
        }
    }
        
    // Bring watermark to FRONT
    containerView.bringSubview(toFront: watermarkLabel)
}

    
// CLOSE ADJUSTMENT TOOL BUTTON =========================
@IBAction func closeAdjustmentButt(_ sender: AnyObject) {
    hideToolView(ADJUSTMENTTOOL_NAME)
}
    
    
    
    
    
// SHARE IMAGE BUTTON =======================================
@IBAction func shareButt(_ sender: AnyObject) {
        let rect:CGRect = containerView.bounds
        UIGraphicsBeginImageContextWithOptions(rect.size, true, 0.0)
        containerView.drawHierarchy(in: containerView.bounds, afterScreenUpdates: false)
        editedImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
    
    goToShareVC()
}
func goToShareVC() {
    let shVC = self.storyboard?.instantiateViewController(withIdentifier: "ShareVC") as! ShareVC
    self.present(shVC, animated: true, completion: nil)
}

    
    
// BACK BUTTON =========================
@IBAction func backButt(_ sender: AnyObject) {
    let alert = UIAlertView(title: APP_NAME,
        message: "Are you sure you want to exit? You'll lose all changes you've made so far.",
        delegate: self,
        cancelButtonTitle: "No",
        otherButtonTitles: "Yes")
        alert.show()
}
// AlertView delegate
func alertView(_ alertView: UIAlertView, clickedButtonAt buttonIndex: Int) {
    if alertView.buttonTitle(at: buttonIndex) == "Yes" {
        let homeVC = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
        homeVC.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        self.present(homeVC, animated: true, completion: nil)
    }
}
    
    
// ANIMATION TO SHOW/HIDE TOOL VIEWS ================================
func showToolView(_ toolName:String) {
    UIView.animate(withDuration: 0.1, delay: 0.0, options: UIViewAnimationOptions.curveLinear, animations: {
    if toolName == FRAMESTOOL_NAME {  self.framesToolView.frame.origin.y = self.view.frame.size.height-116  }
    if toolName == STICKERSTOOL_NAME { self.stickersToolView.frame.origin.y = self.view.frame.size.height-116  }
    if toolName == TEXTURESTOOL_NAME { self.texturesToolView.frame.origin.y = self.view.frame.size.height-116  }
    if toolName == FILTERSTOOL_NAME { self.filtersToolView.frame.origin.y = self.view.frame.size.height-116  }
    if toolName == ADJUSTMENTTOOL_NAME { self.adjustmentToolView.frame.origin.y = self.view.frame.size.height-116  }
        }, completion: { (finished: Bool) in
            self.titleLabel.text = toolName
        });
}
func hideToolView(_ toolName:String) {
    UIView.animate(withDuration: 0.1, delay: 0.0, options: UIViewAnimationOptions.curveLinear, animations: {
    if toolName == FRAMESTOOL_NAME {  self.framesToolView.frame.origin.y = self.view.frame.size.height  }
    if toolName == STICKERSTOOL_NAME { self.stickersToolView.frame.origin.y = self.view.frame.size.height  }
    if toolName == TEXTURESTOOL_NAME { self.texturesToolView.frame.origin.y = self.view.frame.size.height  }
    if toolName == FILTERSTOOL_NAME { self.filtersToolView.frame.origin.y = self.view.frame.size.height  }
    if toolName == ADJUSTMENTTOOL_NAME { self.adjustmentToolView.frame.origin.y = self.view.frame.size.height  }
            
        }, completion: { (finished: Bool) in
            self.titleLabel.text = "Image Editor"
        });
    }
    //===============================================================================
    
    

    
    
    
    
    
    
// MARK: - AdMob BANNER METHODS ========================*/
func initAdMobBanner() {
    if UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad {
        // iPad banner
        adMobBannerView.adSize =  GADAdSizeFromCGSize(CGSize(width: 728, height: 90))
        adMobBannerView.frame = CGRect(x: 0, y: self.view.frame.size.height, width: 728, height: 90)
            
    } else {
        // iPhone banner
        adMobBannerView.adSize =  GADAdSizeFromCGSize(CGSize(width: 320, height: 50))
        adMobBannerView.frame = CGRect(x: 0, y: self.view.frame.size.height, width: 320, height: 50)
    }

    adMobBannerView.adUnitID = ADMOB_UNIT_ID
    adMobBannerView.rootViewController = self
    adMobBannerView.delegate = self
    view.addSubview(adMobBannerView)
        
    let request = GADRequest()
    adMobBannerView.load(request)
}
    
    
    // Hide the banner
    func hideBanner(_ banner: UIView) {
        if banner.isHidden == false {
            UIView.beginAnimations("hideBanner", context: nil)
            // Hide the banner moving it below the bottom of the screen
            banner.frame = CGRect(x: 0, y: self.view.frame.size.height, width: banner.frame.size.width, height: banner.frame.size.height)
            UIView.commitAnimations()
            banner.isHidden = true
        }
    }
    
    // Show the banner
    func showBanner(_ banner: UIView) {
        if banner.isHidden == true {
            UIView.beginAnimations("showBanner", context: nil)
            
            // Move the banner on the bottom of the screen
            banner.frame = CGRect(x: 0, y: (self.view.frame.size.height-44) - banner.frame.size.height,
                width: banner.frame.size.width, height: banner.frame.size.height)
            
            UIView.commitAnimations()
            banner.isHidden = false
        }
    }

    
    // AdMob banner available
    func adViewDidReceiveAd(_ view: GADBannerView!) {
        print("AdMob loaded!")
        showBanner(adMobBannerView)
    }
    
    // NO AdMob banner available
    func adView(_ view: GADBannerView!, didFailToReceiveAdWithError error: GADRequestError!) {
        print("AdMob Can't load ads right now, they'll be available later \n\(error)")
        hideBanner(adMobBannerView)
    }
    

    


override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
}
}
