/*-----------------------------------

- CamFun -

created by FV iMAGINATION © 2015
for CodeCanyon.net

-----------------------------------*/


import UIKit


class HomeVC: UIViewController,
UINavigationControllerDelegate,
UIImagePickerControllerDelegate
{

    /* Views */
    @IBOutlet var logoImage: UIImageView!
    
    @IBOutlet var buttons: [UIButton]!
 
    @IBOutlet var bkgImage: UIImageView!
    
    
    
// Hide the status bar
override var prefersStatusBarHidden : Bool {
    return true;
}
func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
    UIApplication.shared.setStatusBarHidden(true, with: UIStatusBarAnimation.none)
}
    

override func viewWillAppear(_ animated: Bool) {
    // Show a random image as Background
    let randomBKG = Int(arc4random() % 4)
    bkgImage.image = UIImage(named: "bkg\(randomBKG)" )
    print("\(randomBKG)")

}
    
override func viewDidLoad() {
        super.viewDidLoad()
    
    // Round views corners
    logoImage.layer.cornerRadius = 20
    
    
    // Setup layout of buttons
    for butt in buttons {
        butt.layer.cornerRadius = 22
        butt.layer.borderWidth = 0.8
    }
    buttons[0].layer.borderColor = lightGreen.cgColor
    buttons[1].layer.borderColor = red.cgColor
    buttons[2].layer.borderColor = lightGray.cgColor
    buttons[3].layer.borderColor = yellow.cgColor
    
    
    /* CONSOLE LOGS */
    print("rateAppMade: \(rateAppMade)" )
    print("save Orig.Photo: \(saveImageToLibrary)" )
}

    
   
// MARK: - LIBRARY BUTTON
@IBAction func libraryButt(_ sender: AnyObject) {
    if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary;
        imagePicker.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        self.present(imagePicker, animated: true, completion: nil)
    }
}
    
    
    
// MARK: - IMAGE PICKER DELEGATE 
func imagePickerController(_ picker: UIImagePickerController, didFinishPickingImage image: UIImage!, editingInfo: [AnyHashable: Any]!) {
    takenImage = image
    self.dismiss(animated: false, completion: nil)
        
    // Go to Crop Image Controller
    goToCropImage()
}
    
func goToCropImage() {
    let ciVC = self.storyboard?.instantiateViewController(withIdentifier: "CropImageVC") as! CropImageVC
    self.present(ciVC, animated: true, completion: nil)
}


   
// MARK: - INSTAGRAM BUTTON (Get Images from Instagram channel) 
@IBAction func inspirationsButt(_ sender: AnyObject) {
        let instagramURL = URL(string: "instagram://user?username=\(INSTAGRAM_USERNAME)")
        
    if UIApplication.shared.canOpenURL(instagramURL!) {
        UIApplication.shared.openURL(instagramURL!)
    } else {
        let instaWebLink = URL(string: "https://instagram.com/\(INSTAGRAM_USERNAME)/")
        UIApplication.shared.openURL(instaWebLink!)
    }
}
    
 
    
    
// MARK: - OPEN TERM OF USE HTML PAGE
@IBAction func touButt(_ sender: AnyObject) {
    let touVC = self.storyboard?.instantiateViewController(withIdentifier: "TermsOfUse") as! TermsOfUse
    present(touVC, animated: true, completion: nil)
}
    
    
    
    
override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
}

}
