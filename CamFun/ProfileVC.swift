/*-----------------------------------

- CamFun -

created by FV iMAGINATION © 2015
for CodeCanyon.net

-----------------------------------*/


import UIKit
import Parse


// CUSTOM CELL
class MyPostCell: UICollectionViewCell {
    /* Views */
    @IBOutlet var postImageView: UIImageView!
    @IBOutlet var likesLabel: UILabel!
    @IBOutlet var positionLabel: UILabel!
}








// MARK: - PROFILE CONTROLLER
class ProfileVC: UIViewController,
UITextFieldDelegate,
UIAlertViewDelegate,
UINavigationControllerDelegate,
UIImagePickerControllerDelegate,
UICollectionViewDataSource,
UICollectionViewDelegate,
UICollectionViewDelegateFlowLayout
{

    /* Views */
    @IBOutlet var userAvatarImage: UIImageView!
    @IBOutlet var usernameTxt: UITextField!
    @IBOutlet var emailTxt: UITextField!
    @IBOutlet var updateProfOutlet: UIButton!
    @IBOutlet var myPostsCollView: UICollectionView!
    
    
    /* Variables */
    var postsArray = [PFObject]()
    var cellSize: CGSize?
    
    
    
override var prefersStatusBarHidden : Bool {
    return true
}
func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
    UIApplication.shared.setStatusBarHidden(true, with: UIStatusBarAnimation.none)
}
    
    
override func viewDidAppear(_ animated: Bool) {
    loadPosts()
}

override func viewDidLoad() {
        super.viewDidLoad()
 
    // Round button corners
    updateProfOutlet.layer.cornerRadius = 20
    
    
    // Set size of Post Cells
    if UIScreen.main.bounds.size.width == 320 {
        //iPhone 4 / 5
        cellSize = CGSize(width: 95, height: 95)
    } else if UIScreen.main.bounds.size.width == 375 {
        // iPhone 6
        cellSize = CGSize(width: 115, height: 115)
    } else if UIScreen.main.bounds.size.width == 414 {
        // iPhone 6+
        cellSize = CGSize(width: 128, height: 128)
    } else if UIScreen.main.bounds.size.width == 768 {
        // iPad
        cellSize = CGSize(width: 240, height: 240)
    }
    
    
    // Round avatar image corners
    userAvatarImage.layer.cornerRadius = userAvatarImage.bounds.size.width/2
    
    // Get current User's details and Avatar
    let currentUser = PFUser.current()!
    
    usernameTxt.text = PFUser.current()!.username
    emailTxt.text = PFUser.current()!.email
    
    let avatarImage = currentUser[USER_AVATAR] as? PFFile
    avatarImage?.getDataInBackground(block: { (imageData, error) in
        if error == nil {
            if let imageData = imageData {  self.userAvatarImage.image =  UIImage(data:imageData)
    }}})

}

    
// MARK: - LOAD POSTS METHOD
func loadPosts() {
    showHUD()
    postsArray.removeAll()
   
    let query = PFQuery(className:POSTS_CLASS_NAME)
    query.whereKey(POSTS_USER, equalTo: PFUser.current()!)
    query.order(byDescending: POSTS_LIKES)
    
    // Execute query
    query.findObjectsInBackground {(objects, error) -> Void in
        if error == nil {
            self.postsArray = objects!
            self.myPostsCollView.reloadData()
            self.hideHUD()
        
            // Add  label to notify that you haven't post anything yet in the WOF
            if self.postsArray.count == 0 {
                let noPostsYetLabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 44))
                noPostsYetLabel.text = "You haven't posted any picture yet"
                noPostsYetLabel.font = UIFont(name: "DancingScript", size: 20)
                noPostsYetLabel.textAlignment = NSTextAlignment.center
                noPostsYetLabel.textColor = UIColor.white
                noPostsYetLabel.center = CGPoint(x: self.myPostsCollView.frame.size.width/2, y: self.myPostsCollView.frame.size.height/2)
                self.myPostsCollView.addSubview(noPostsYetLabel)
            }
            
            
        } else {
            self.simpleAlert("\(error!.localizedDescription)")
            self.hideHUD()
    }}
        
}

    
    
// MARK: - CHANGE AVATAR BUTTON
@IBAction func changeAvatarButt(_ sender: AnyObject) {
    let alert = UIAlertView(title: APP_NAME,
    message: "Choose an image",
    delegate: self,
    cancelButtonTitle: "Cancel",
    otherButtonTitles: "Camera",
                       "Photo Library")
    alert.show()
}
    
// AlertView delegate
func alertView(_ alertView: UIAlertView, clickedButtonAt buttonIndex: Int) {
    if alertView.buttonTitle(at: buttonIndex) == "Camera" {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera;
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }

        
    } else if alertView.buttonTitle(at: buttonIndex) == "Photo Library" {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary;
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }

    }
}
    
// Image Picker Delegate
func imagePickerController(_ picker: UIImagePickerController, didFinishPickingImage image: UIImage!, editingInfo: [AnyHashable: Any]!) {
    avatarImg = image
    userAvatarImage.image = image
    dismiss(animated: true, completion: nil)
}
    
    
// MARK: - UPDATE PROFILE BUTTON
@IBAction func updateProfileButt(_ sender: AnyObject) {
    showHUD()
    let currentUser = PFUser.current()
    
    currentUser!.username = usernameTxt.text
    currentUser!.email = emailTxt.text
    
    if userAvatarImage.image != nil {
        let imageData = UIImageJPEGRepresentation(userAvatarImage.image!, 0.2)
        let imageFile = PFFile(name:"avatar.jpg", data:imageData!)
        currentUser![USER_AVATAR] = imageFile
    }

    // Saving block
    currentUser!.saveInBackground {(succ, error) -> Void in
     if error == nil {
        self.simpleAlert("Your profile has been updated!")
        self.hideHUD()
     } else {
        self.simpleAlert("\(error!.localizedDescription)")
        self.hideHUD()
    }}
    
}
    
    
    
// MARK: - LOGOUT BUTTON
@IBAction func logoutButt(_ sender: AnyObject) {
    
    let alert = UIAlertController(title: APP_NAME,
        message: "Are you sure you want to logout?",
        preferredStyle: .alert)
    
    let ok = UIAlertAction(title: "Logout", style: UIAlertActionStyle.default, handler: { (action) -> Void in
        self.showHUD()
        
        PFUser.logOutInBackground { (error) -> Void in
            if error == nil {
               self.dismiss(animated: true, completion: nil)
                self.hideHUD()
        }}
    })
    
    let cancel = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: { (action) -> Void in })
    
    alert.addAction(ok); alert.addAction(cancel)
    present(alert, animated: true, completion: nil)
}

    
// MARK: - DISMISS BUTTON 
@IBAction func dismissButt(_ sender: AnyObject) {
    dismiss(animated: true, completion: nil)
}
  


 
    
    
// MARK: - COLLECTION VIEW DELEGATES
func numberOfSections(in collectionView: UICollectionView) -> Int {
    return 1
}
    
func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return postsArray.count
}
    
func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MyPostCell", for: indexPath) as! MyPostCell
        
    var posts = PFObject(className: POSTS_CLASS_NAME)
    posts = postsArray[indexPath.row]
    
    // Get Post image
    let postImage = posts[POSTS_IMAGE_FILE] as? PFFile
    postImage?.getDataInBackground(block: { (imageData, error) in
    if error == nil {
        if let imageData = imageData {
            cell.postImageView.image = UIImage(data:imageData)
    }}})
    
    // Get post Likes
    if posts[POSTS_LIKES] == nil  {  cell.likesLabel.text = "0 Likes"  }
    else {  cell.likesLabel.text = "\( posts.object(forKey: POSTS_LIKES)! ) Likes"  }
    
    // Get position in WOF
    if posts[POSTS_POSITION] == nil  {  cell.positionLabel.text = "New Entry"  }
    else {  cell.positionLabel.text = "Pos: \( posts.object(forKey: POSTS_POSITION)! )"  }
    
    return cell
}
    
func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    return cellSize!
}
    
  

    
    

// MARK: -  TEXTFIELD DELEGATES
func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    if textField == usernameTxt {  emailTxt.becomeFirstResponder()  }
    if textField == emailTxt  {  emailTxt.resignFirstResponder()  }
        
return true
}
    
    
    
    
    
override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
