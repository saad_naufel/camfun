/*-----------------------------------

- CamFun -

created by FV iMAGINATION © 2015
for CodeCanyon.net

-----------------------------------*/


import UIKit
import Parse
import MessageUI



// MARK: - CUSTOM POST CELL
class PostCell: UICollectionViewCell {
    
    /* Views */
    @IBOutlet var positionLabel: UILabel!
    
    @IBOutlet var postImageView: UIImageView!
    @IBOutlet var userAvatar: UIImageView!
    
    @IBOutlet var usernameLabel: UILabel!
    @IBOutlet var postDateLabel: UILabel!
    
    @IBOutlet var likesLabel: UILabel!
    
    @IBOutlet var reportButt: UIButton!
    
}





// MARK: - WALL OF FAME CONTROLLER
class WallOfFameVC: UIViewController,
UICollectionViewDataSource,
UICollectionViewDelegate,
UICollectionViewDelegateFlowLayout,
MFMailComposeViewControllerDelegate,
UIAlertViewDelegate
{

    /* Views */
    @IBOutlet var postsCollView: UICollectionView!
    
    
    /* Variables */
    var cellSize = CGSize(width: 0, height: 0)
    var postsArray = [PFObject]()
    var postLiked = -1
    
    
    

override var prefersStatusBarHidden : Bool {
    return true
}

override func viewDidAppear(_ animated: Bool) {
    loadPosts()
}
    
override func viewDidLoad() {
        super.viewDidLoad()

    // Set size of Post Cells
    if UIScreen.main.bounds.size.width == 320 {
        //iPhone 4 / 5
        cellSize = CGSize(width: 320, height: 450)
    } else if UIScreen.main.bounds.size.width == 375 {
        // iPhone 6
        cellSize = CGSize(width: 375, height: 500)
    } else if UIScreen.main.bounds.size.width == 414 {
        // iPhone 6+
        cellSize = CGSize(width: 414, height: 540)
    } else if UIScreen.main.bounds.size.width == 768 {
        // iPad
        cellSize = CGSize(width: 768, height: 900)
    }
    
    
}

    
    
    
// MARK: - LOAD POSTS
func loadPosts() {
    showHUD()
    postsArray.removeAll()
    
    let query = PFQuery(className:POSTS_CLASS_NAME)
    query.includeKey(POSTS_USER)
    query.order(byDescending: POSTS_LIKES)
    
    // Parse query
    query.findObjectsInBackground {(objects, error) -> Void in
        if error == nil {
            self.postsArray = objects!
            self.postsCollView.reloadData()
            self.hideHUD()

        } else {
            self.simpleAlert("\(error!.localizedDescription)")
            self.hideHUD()
    }}
}

    
    
   
// MARK: - COLLECTION VIEW DELEGATES
func numberOfSections(in collectionView: UICollectionView) -> Int {
    return 1
}
    
func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return postsArray.count
}
    
func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PostCell", for: indexPath) as! PostCell
    
    // Resize post image accordingly to the Device used
    if UIScreen.main.bounds.size.width == 320 {
        //iPhone 4 / 5
        cell.postImageView.frame = CGRect(x: 0, y: 44, width: 320, height: 320)
    } else if UIScreen.main.bounds.size.width == 375 {
        // iPhone 6
        cell.postImageView.frame = CGRect(x: 0, y: 44, width: 375, height: 375)
    } else if UIScreen.main.bounds.size.width == 414 {
        // iPhone 6+
        cell.postImageView.frame = CGRect(x: 0, y: 44, width: 414, height: 414)
    } else if UIScreen.main.bounds.size.width == 768 {
        // iPad
        cell.postImageView.frame = CGRect(x: 0, y: 44, width: 768, height: 768)
    }
    
    // Assign a TAG to the Report button
    cell.reportButt.tag = indexPath.row
    
    
    
    // Populate cells:
    DispatchQueue.main.async(execute: {
        
        // Create a POSTS Object
        var posts = PFObject(className: POSTS_CLASS_NAME)
        posts = self.postsArray[indexPath.row]
    
        // Get userPointer
        let userPointer = posts[POSTS_USER] as! PFUser
        userPointer.fetchIfNeededInBackground(block: { (object, error) in
    
            // Get Post image
            let postImage = posts[POSTS_IMAGE_FILE] as? PFFile
            postImage?.getDataInBackground(block: { (imageData, error) in
                if error == nil {
                    if let imageData = imageData {  cell.postImageView.image = UIImage(data:imageData)
            }}})
    
        
            // Get User Avatar image
            let avatarImage = userPointer[USER_AVATAR] as? PFFile
            avatarImage?.getDataInBackground(block: { (imageData, error) in
                if error == nil {
                    if let imageData = imageData {  cell.userAvatar.image = UIImage(data:imageData)
            }}})
    
        
            // Round user image corners
            cell.userAvatar.layer.cornerRadius = cell.userAvatar.bounds.size.width/2
    
            // Get username
            cell.usernameLabel.text = "\(userPointer[USER_USERNAME]!)"
    
            // Get post date
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MMM dd yyyy"
            let dateStr = dateFormatter.string(from: posts.createdAt!)
            cell.postDateLabel.text = dateStr
    
            // Keep tracking of the picture's position
            let positionCount = indexPath.row
            cell.positionLabel.text = "\( positionCount+1 )"
            posts[POSTS_POSITION] = positionCount+1
    
            // Show likes amount
            if posts[POSTS_LIKES] == nil {  cell.likesLabel.text = "0 Likes"  }
            else {  cell.likesLabel.text = "\(posts[POSTS_LIKES]!) Likes"  }
        })
       
    }) // end dispatch_asynch

    
return cell
}
    
func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    return cellSize
}
    
    
// MARK: - TAP AN IMAGE TO LIKE IT
func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    var aPost = PFObject(className: POSTS_CLASS_NAME)
    aPost = postsArray[indexPath.row]
    
    
    // USER IS LOGGED IN -> LIKE THE IMAGE HE TAPPED
    if PFUser.current() != nil {
        
        // LIKE POSTS OF OTHER USERS
        let userPointer = aPost[POSTS_USER] as! PFUser
        userPointer.fetchIfNeededInBackground(block: { (user, error) in
            
            let postUsername = "\(userPointer[USER_USERNAME]!)"
            if postUsername != PFUser.current()!.username {
                // Popup a like Image
                let likeView = UIView(frame: self.view.frame)
                likeView.backgroundColor = UIColor.black
                likeView.alpha = 0.6
                let likeImage = UIImageView(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
                likeImage.center = CGPoint(x: likeView.frame.size.width/2, y: likeView.frame.size.height/2)
                likeImage.image = UIImage(named: "likeImage")
                likeView.addSubview(likeImage)
                self.view.addSubview(likeView)
                
                UIView.animate(withDuration: 0.1, delay: 0.4, options: UIViewAnimationOptions.curveLinear, animations: {
                    likeView.alpha = 0
                    }, completion: { (finished: Bool) in
                        likeView.removeFromSuperview()
                })
            
                // Add 1 Like to the selected Post
                aPost.incrementKey(POSTS_LIKES, byAmount: 1)
    
                // Saving block
                aPost.saveInBackground {(succ, error) -> Void in
                    if error == nil {
                        self.loadPosts()
                        self.postLiked = indexPath.row
                    } else {
                        self.simpleAlert("\(error!.localizedDescription)")
                }}
            
                
            // YOU CANNOT LIKE YOUR OWN POSTS!
            } else { self.simpleAlert("You cannot like your own posts") }
        
        })

     
        
        
    // USER IS NOT LOGGED IN, CAN'T LIKE ANY PICTURE, MUST LOGIN
    } else {
        simpleAlert("You must login to like a post.\nGo to Settings and Login into your Profile, or create a new one")
    }
}

    

    
// MARK: - REPORT POST'S IMAGE BUTTON
@IBAction func reportButt(_ sender: AnyObject) {
    var posts = PFObject(className: POSTS_CLASS_NAME)
    posts = postsArray[sender.tag]
    let postUsername = "\(posts[POSTS_USERNAME]!)"
    let postPosition = posts[POSTS_POSITION] as! Int
    
    let mailComposer: MFMailComposeViewController = MFMailComposeViewController()
    mailComposer.mailComposeDelegate = self
    mailComposer.setSubject(REPORT_EMAIL_SUBJECT)
    mailComposer.setMessageBody("Hello,\npost by <strong>\(postUsername)</strong>, at position <strong>\(postPosition)</strong> is inappropriate and/or it shows offensive contents.", isHTML: true)
    mailComposer.setToRecipients([REPORT_EMAIL_ADDRESS])
    present(mailComposer, animated: true, completion: nil)
}


// Email delegate
func mailComposeController(_ controller:MFMailComposeViewController, didFinishWith result:MFMailComposeResult, error:Error?) {
    var outputMessage = ""
    switch result.rawValue {
    case MFMailComposeResult.cancelled.rawValue:
        outputMessage = "Mail cancelled"
    case MFMailComposeResult.saved.rawValue:
        outputMessage = "Mail saved"
    case MFMailComposeResult.sent.rawValue:
        outputMessage = "Mail sent"
    case MFMailComposeResult.failed.rawValue:
        outputMessage = "Something went wrong with sending Mail, try again later."
    
    default: break }
    
    simpleAlert(outputMessage)
    dismiss(animated: false, completion: nil)
}


    

// MARK: - DISMISS BUTTON
@IBAction func dismissButt(_ sender: AnyObject) {
    dismiss(animated: true, completion: nil)
}

    
// MARK: - REFRESH BUTTON
@IBAction func refreshButt(_ sender: AnyObject) {
    loadPosts()
}
    
    
    
    
    

    
override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
