/*-----------------------------------

- CamFun -

created by FV iMAGINATION © 2015
for CodeCanyon.net

-----------------------------------*/


import UIKit
import Parse


class LoginVC: UIViewController,
UIAlertViewDelegate,
UITextFieldDelegate
{
    
    /* Views */
    @IBOutlet var containerScrollView: UIScrollView!
    
    @IBOutlet var usernameTxt: UITextField!
    @IBOutlet var passwordTxt: UITextField!
    @IBOutlet weak var contView: UIView!
    
    @IBOutlet var buttons: [UIButton]!
    
    
    
    
    
override var prefersStatusBarHidden : Bool {
        return true
}
override func viewWillAppear(_ animated: Bool) {
    // Check if current User is logged in
    if PFUser.current() != nil {
        dismiss(animated: false, completion: nil)
    }
}
    
override func viewDidLoad() {
    super.viewDidLoad()
    
    contView.frame.size.width = view.frame.size.width
    
    // Round buttons corners
    for button in buttons {  button.layer.cornerRadius = 20  }
    
    // Setup containerScrolView
    containerScrollView.contentSize = CGSize(width: containerScrollView.frame.size.width, height: 300)
    
    if UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad {
        containerScrollView.isScrollEnabled = false
    }
}
    
    


    
// MARK: - TEXTFIELD DELEGATES
func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    if textField == usernameTxt {  passwordTxt.becomeFirstResponder()  }
    if textField == passwordTxt  {  passwordTxt.resignFirstResponder()  }

    return true
}
    
    
// MARK: - TAP TO DISMISS THE KEYBOARD
@IBAction func tapToDismissKeyboard(_ sender: UITapGestureRecognizer) {
    dismissKeyboard()
}
func dismissKeyboard() {
    usernameTxt.resignFirstResponder()
    passwordTxt.resignFirstResponder()
}
 
    
    
    
// MARK: - LOGIN BUTTON
@IBAction func loginButt(_ sender: AnyObject) {
    showHUD()
    dismissKeyboard()
    
    PFUser.logInWithUsername(inBackground: usernameTxt.text!, password:passwordTxt.text!) {(user, error) -> Void in
        // Login successfull
        if user != nil {
            self.dismiss(animated: true, completion: nil)
            self.hideHUD()
            
        // Login failed, try again
        } else {
            let alert = UIAlertView(title: APP_NAME,
            message: "Loing Error",
            delegate: self,
            cancelButtonTitle: "Retry",
            otherButtonTitles: "Sign Up")
            alert.show()
            
            self.hideHUD()
    }}

}
// AlertView delegate
func alertView(_ alertView: UIAlertView, clickedButtonAt buttonIndex: Int) {
    if alertView.buttonTitle(at: buttonIndex) == "Sign Up" {
        signUpButt(self)
    }
    
    if alertView.buttonTitle(at: buttonIndex) == "Reset Password" {
        PFUser.requestPasswordResetForEmail(inBackground: "\(alertView.textField(at: 0)!.text!)")
        showNotifAlert()
    }
}
    
    
// MARK: - SIGN UP BUTTON
@IBAction func signUpButt(_ sender: AnyObject) {
    let signupVC = self.storyboard?.instantiateViewController(withIdentifier: "SignUpVC") as! SignUpVC
    present(signupVC, animated: true, completion: nil)
}

    
// MARK: - FORGOT PASSWORD BUTTON
@IBAction func forgotPasswButt(_ sender: AnyObject) {
    let alert = UIAlertView(title: APP_NAME,
    message: "Type the email address you've used to register on \(APP_NAME) to reset your password",
    delegate: self,
    cancelButtonTitle: "Cancel",
    otherButtonTitles: "Reset Password")
    alert.alertViewStyle = UIAlertViewStyle.plainTextInput
    alert.show()
}
    
    
func showNotifAlert() {
    simpleAlert("You will receive an email shortly with a link to reset your password")
}
    
    
    
// MARK: - DISMISS BUTTON
@IBAction func dismissButt(_ sender: AnyObject) {
    dismiss(animated: true, completion: nil)
}

    
    
    
    
    
override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
}
}

