/*-----------------------------------

- CamFun -

created by FV iMAGINATION © 2015
for CodeCanyon.net

-----------------------------------*/


import UIKit
import Parse


class SignUpVC: UIViewController,
UITextFieldDelegate
{
    
    /* Views */
    @IBOutlet var containerScrollView: UIScrollView!
    @IBOutlet var usernameTxt: UITextField!
    @IBOutlet var passwordTxt: UITextField!
    @IBOutlet var emailTxt: UITextField!
    
    @IBOutlet var signupOutlet: UIButton!
    
    @IBOutlet weak var contView: UIView!

    
    
override var prefersStatusBarHidden : Bool {
        return true
}
override func viewDidLoad() {
    super.viewDidLoad()

    contView.frame.size.width = view.frame.size.width
    
    
    // Round button corners
    signupOutlet.layer.cornerRadius = 20
    
    
    // Setup containerScrollView
    containerScrollView.contentSize = CGSize(width: containerScrollView.frame.size.width, height: 300)
        
    if UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad {
        containerScrollView.isScrollEnabled = false
    }
        
}
    
    
// MARK: - TAP TO DISMISS THE KEYBOARD
@IBAction func tapToDismissKeyboard(_ sender: UITapGestureRecognizer) {
    dismisskeyboard()
}
  
func dismisskeyboard() {
    usernameTxt.resignFirstResponder()
    passwordTxt.resignFirstResponder()
    emailTxt.resignFirstResponder()
}
    
    
    
// MARK: - SIGNUP BUTTON
@IBAction func signUpButt(_ sender: AnyObject) {
    showHUD()
    dismisskeyboard()
    
    let userForSignUp = PFUser()
    userForSignUp.username = usernameTxt.text
    userForSignUp.password = passwordTxt.text
    userForSignUp.email = emailTxt.text
    
    let imageData = UIImageJPEGRepresentation(UIImage(named: "logo")!, 0.5)
    let imageFile = PFFile(name:"userAvatar.png", data:imageData!)
    userForSignUp[USER_AVATAR] = imageFile
    
    // Saving block
    userForSignUp.signUpInBackground { (succeeded, error) -> Void in
        if error == nil {
            self.hideHUD()
            self.dismiss(animated: true, completion: nil)
                
        } else {
            self.simpleAlert("\(error!.localizedDescription)")
            self.hideHUD()
    }}
}
    
    
    
// MARK: - TEXTFIELD DELEGATES
func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    if textField == usernameTxt {  passwordTxt.becomeFirstResponder()  }
    if textField == passwordTxt {  emailTxt.becomeFirstResponder()  }
    if textField == emailTxt {  emailTxt.resignFirstResponder()  }
        
return true
}
    
    
// mark: - dismiss button
@IBAction func dismissButt(_ sender: AnyObject) {
    dismiss(animated: true, completion: nil)
}
  
   

    
override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
}
}

