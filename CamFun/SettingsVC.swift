/*-----------------------------------

- CamFun -

created by FV iMAGINATION © 2015
for CodeCanyon.net

-----------------------------------*/


import UIKit
import MessageUI
import Parse



class SettingsVC: UIViewController,
MFMailComposeViewControllerDelegate
{

    /* Views */
    @IBOutlet var saveToLibrarySwitch: UISwitch!
    
    
    
    
override var prefersStatusBarHidden : Bool {
    return true
}
override func viewDidLoad() {
        super.viewDidLoad()
    
    saveToLibrarySwitch.setOn(saveImageToLibrary, animated: true)
    
}
 
     
    
    
// MARK: - RATE APP BUTTON
@IBAction func rateAppButt(_ sender: AnyObject) {
    if !rateAppMade {
    let reviewURL = URL(string: "http://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id=\(APP_ID)")
    UIApplication.shared.openURL(reviewURL!)
        rateAppMade = true
        DEFAULTS.set(rateAppMade, forKey: "rateAppMade")
        
    } else { simpleAlert("You have already rated \(APP_NAME) on the App Store. Thanks!") }
}
   
    
    
// MARK: - SEND FEEDBACK BUTTON
@IBAction func sendFeedbackButt(_ sender: AnyObject) {
    let mailComposer: MFMailComposeViewController = MFMailComposeViewController()
    mailComposer.mailComposeDelegate = self
    let supportAddress = [MY_SUPPORT_EMAIL_ADDRESS]
    mailComposer.setToRecipients(supportAddress)
    mailComposer.setSubject(SUPPORT_MAIL_SUBJECT)
    mailComposer.setMessageBody(SUPPORT_MESSAGE_BODY, isHTML: true)
    self.present(mailComposer, animated: true, completion: nil)
}
// Email results
func mailComposeController(_ controller:MFMailComposeViewController, didFinishWith result:MFMailComposeResult, error:Error?) {
    var mailOutput = ""
        switch result.rawValue {
        case MFMailComposeResult.cancelled.rawValue:
            mailOutput = "Email cancelled"
        case MFMailComposeResult.sent.rawValue:
            mailOutput = "Thanks for sending your feedback, we'll get back to you asap!"
        case MFMailComposeResult.failed.rawValue:
            mailOutput = "Something went wrong, come back later and try emailing us again."
        default:break
    }
    
    simpleAlert(mailOutput)
    dismiss(animated: false, completion: nil)
}

    
    
// MARK: - SWITCH TO SAVE ORIGINAL PHOTO TO LIBRARY
@IBAction func saveToLibraryChanged(_ sender: UISwitch) {
    if sender.isOn {  saveImageToLibrary = true }
    else {  saveImageToLibrary = false }
    
    DEFAULTS.set(saveImageToLibrary, forKey: "saveImageToLibrary")
}
    
    
    
// MARK: - LOGIN BUTTON
@IBAction func loginButt(_ sender: AnyObject) {
    if PFUser.current() == nil {
        let loginVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        self.present(loginVC, animated: true, completion: nil)
        
    } else {
        simpleAlert("You're already logged in!")
    }

}
    
    
    
// MARK: - SIGN UP BUTTON
@IBAction func signupButt(_ sender: AnyObject) {
    if PFUser.current() == nil {
        let signVC = self.storyboard?.instantiateViewController(withIdentifier: "SignUpVC") as! SignUpVC
        self.present(signVC, animated: true, completion: nil)
        
    } else {
        simpleAlert("You're already logged in!")
    }
}




// MARK: - ENTER YOUR WOF PROFILE
@IBAction func myProfileButt(_ sender: AnyObject) {
    if PFUser.current() == nil {
        let loginVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        self.present(loginVC, animated: true, completion: nil)
    } else {
        let profVC = self.storyboard?.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
        self.present(profVC, animated: true, completion: nil)
    }
    
}
    
    
    
// MARK: - DISMISS BUTTON
@IBAction func dismissButt(_ sender: AnyObject) {
    dismiss(animated: true, completion: nil)
}
    
    
    
override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
}
}
