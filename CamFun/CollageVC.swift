/*-----------------------------------

- CamFun -

created by FV iMAGINATION © 2015
for CodeCanyon.net

-----------------------------------*/


import UIKit



// MARK: - CUSTOM COLLAGE CELL
class CollageCell: UICollectionViewCell {
    /* Views */
    @IBOutlet var thumbnail: UIImageView!
}






// MARK: - COLLAGE CONTROLLER
class CollageVC: UIViewController,
UICollectionViewDataSource,
UICollectionViewDelegate,
UICollectionViewDelegateFlowLayout,
UINavigationControllerDelegate
{
    
    
    
// Hide the status bar 
override var prefersStatusBarHidden : Bool {
    return true;
}
    
override func viewDidLoad() {
        super.viewDidLoad()
    
}

    
// MARK: - DISMISS BUTTON
@IBAction func dismissButt(_ sender: AnyObject) {
    let homeVC = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
    homeVC.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
    self.present(homeVC, animated: true, completion: nil)
}
    
    
    
    
   
// MARK: - COLLECTION VIEW DELEGATES
func numberOfSections(in collectionView: UICollectionView) -> Int {
    return 1
}
    
func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return 16
}
    
func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollageCell", for: indexPath) as! CollageCell
    
    cell.thumbnail.image = UIImage(named: "c\(indexPath.row)")

    
return cell
}
    
func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    return CGSize(width: 95, height: 95)
}
    
func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
   
    collageFrameNr = indexPath.row
    
    let ecVC = self.storyboard?.instantiateViewController(withIdentifier: "EditCollageVC") as! EditCollageVC
    ecVC.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
    self.present(ecVC, animated: true, completion: nil)
}
    

    
    
    
    
override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
}
}
